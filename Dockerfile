FROM openjdk:17
ADD target/*.jar bikes_back
ENTRYPOINT ["java", "-jar","bikes_back"]
EXPOSE 8080
