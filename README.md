This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

# ***Bikes Backend***

## Presentation

### Bean

### Key / Converter

### Repository > Service > Controller

### GenericController

### GenericRepository
 >StockRepository => @Query

### GenericService
>getAll (page, size, sortBy)

### Lombok

### SLF4J / Logs (Fichier)

### Test Unitaires
		@BeforeAll : dropAll 

## Build :

### Files
    Dockerfile
    docker-compose.yml
    env
        Network

### Process
    cd \Bikes\bikes_back

    To test without docker (with embedded h2 database)
        mvn spring-boot:run "-Dspring-boot.run.profiles=h2"
[application-h2.properties](/src/main/resources/application-h2.properties)

[data.sql](/src/main/resources/data.sql)

[schema.sql](/src/main/resources/schema.sql)

[Console](http://localhost:8080/h2-console)


    To start backend container (binded with Postgres container)
        mvn clean install -DskipTests
        docker-compose up
    To stop backend Container 
        docker-compose down