package org.srevel.bikes.exception;

public class NotFoundException extends RuntimeException  {

        private String message;

        public NotFoundException(String message) {
            super(message);
            this.message = message;
        }

        public NotFoundException() {

        }


    @Override
    public String toString() {
        return "NotFoundException{" +
                "timestamp : 2021-12-28T17:08:41.495+00:00"+
                "status : 500"+
                "error : NotFoundException"+
                "path: /customers/20000"+
                "message='" + message + '\'' +
                '}';
    }
}
