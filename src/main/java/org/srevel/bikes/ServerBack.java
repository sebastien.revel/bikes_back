package org.srevel.bikes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.srevel.bikes")
@EnableAutoConfiguration
@Slf4j(topic ="ServerBack")
@SpringBootApplication
public class ServerBack {

    public static void main(String[] args) {

        log.info("Server Back Started !! ");

        SpringApplication.run(ServerBack.class, args);
    }
}