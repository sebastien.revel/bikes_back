package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Product;
import org.srevel.bikes.repository.ProductRepository;

@Service
@Slf4j (topic = "OrderService")
public class ProductService extends GenericService <Product, Long>  {

    @Autowired
    ProductRepository repository;

    @Override
    ProductRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

    public Iterable<Product> searchByNameLike(String pattern) {
        return getRepository().searchByNameLike(pattern);
    }
}