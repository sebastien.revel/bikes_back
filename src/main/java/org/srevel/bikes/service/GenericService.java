package org.srevel.bikes.service;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.srevel.bikes.repository.GenericRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public abstract class GenericService <T, D> {

    abstract GenericRepository<T, D> getRepository() ;
    abstract Logger getLog() ;

    public Optional<T> findById(D id) {
        return getRepository().findById(id);
    }

    public Iterable<T> findAll() {
        return getRepository().findAll();
    }

    public long count() {
        return getRepository().count();
    }

    public Iterable<T> findAll(Sort sort) {
        return getRepository().findAll(sort);
    }

    public Page<T> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    public <S extends T> S save(S entity) {
        return getRepository().save(entity);
    }

    public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
        return getRepository().saveAll(entities);
    }

    public boolean existsById(D id) {
        return getRepository().existsById(id);
    }

    public Iterable<T> findAllById(Iterable<D> ids){
        return getRepository().findAllById(ids);
    }

    public void deleteById(D id){
        getRepository().deleteById(id);
    }

    public void delete(T entity){
        getRepository().delete(entity);
    }

    public void deleteAllById(Iterable<? extends D> ids){
        getRepository().deleteAllById(ids);
    }

    public  void deleteAll(Iterable<? extends T> entities){
        getRepository().deleteAll(entities);
    }

    public void deleteAll(){
        getRepository().deleteAll();
    }



    public List<T> getAll (Integer pageNo, Integer pageSize , List<Sort.Order> sortBy) {
        if (Objects.isNull(pageNo)) {
            pageNo=0;
        }
        if (Objects.isNull(pageSize)) {
            pageSize=Integer.MAX_VALUE;
        }
        if (Objects.isNull(sortBy)) {
            sortBy=new ArrayList<Sort.Order> ();
        }

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<T> pagedResult = getRepository().findAll(paging);

        if (pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }

}