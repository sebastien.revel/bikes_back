package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.OrderItem;
import org.srevel.bikes.bean.key.OrderItemId;
import org.srevel.bikes.repository.OrderItemRepository;

@Service
@Slf4j (topic = "OrderService")
public class OrderItemService extends GenericService <OrderItem, OrderItemId>  {

    @Autowired
    OrderItemRepository repository;

    @Override
    OrderItemRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

}