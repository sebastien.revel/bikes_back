package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Customer;
import org.srevel.bikes.repository.CustomerRepository;

@Service
@Slf4j (topic = "CustomerService")
public class CustomerService extends GenericService <Customer, Long>  {

    @Autowired
    CustomerRepository  repository;

    @Override
    CustomerRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }


    public Iterable<Customer> searchByFirstnameOrLastnameLike(String pattern) {
        return getRepository().searchByFirstnameOrLastnameLike(pattern);
    }

}