package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Stock;
import org.srevel.bikes.bean.key.StockId;
import org.srevel.bikes.repository.StockRepository;

@Service
@Slf4j (topic = "OrderService")
public class StockService extends GenericService <Stock, StockId>  {

    @Autowired
    StockRepository repository;

    @Override
    StockRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

    public Iterable<Stock> searchByStoreId(Long storeId) {
        return getRepository().searchByStoreId(storeId);
    }

    public Iterable<Stock> searchByProductId(Long productId) {
        return getRepository().searchByProductId(productId);
    }
}