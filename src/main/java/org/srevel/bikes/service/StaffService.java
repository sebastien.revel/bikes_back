package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Staff;
import org.srevel.bikes.repository.StaffRepository;

@Service
@Slf4j (topic = "OrderService")
public class StaffService extends GenericService <Staff, Long>  {

    @Autowired
    StaffRepository repository;

    @Override
    StaffRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

    public Iterable<Staff> searchByFirstnameOrLastnameLike(String pattern) {
        return getRepository().searchByFirstnameOrLastnameLike(pattern);
    }
}