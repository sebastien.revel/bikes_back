package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Brand;
import org.srevel.bikes.repository.BrandRepository;

@Service
@Slf4j (topic = "OrderService")
public class BrandService extends GenericService <Brand, Long>  {

    @Autowired
    BrandRepository repository;

    @Override
    BrandRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

}