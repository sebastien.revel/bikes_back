package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.repository.OrderRepository;
import org.srevel.bikes.bean.Order;

import java.util.Optional;

@Service
@Slf4j (topic = "OrderService")
public class OrderService extends GenericService <Order, Long>  {

    @Autowired
    OrderRepository repository;

    @Override
    OrderRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

    public Optional<Order> findById(Long id) {
        return Optional.of(getRepository().findById(id).get());
    }

}