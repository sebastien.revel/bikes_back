package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Category;
import org.srevel.bikes.repository.CategoryRepository;

@Service
@Slf4j (topic = "OrderService")
public class CategoryService extends GenericService <Category, Long>  {

    @Autowired
    CategoryRepository repository;

    @Override
    CategoryRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

}