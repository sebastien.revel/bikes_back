package org.srevel.bikes.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.srevel.bikes.bean.Store;
import org.srevel.bikes.repository.StoreRepository;

@Service
@Slf4j (topic = "OrderService")
public class StoreService extends GenericService <Store, Long>  {

    @Autowired
    StoreRepository repository;

    @Override
    StoreRepository getRepository() {
        return repository;
    }

    @Override
    Logger getLog() {
        return log;
    }

}