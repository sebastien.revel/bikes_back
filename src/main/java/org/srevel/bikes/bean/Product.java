package org.srevel.bikes.bean;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "products")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id","productName","modelYear","listPrice", "category", "brand"})
@ToString(of= {"id","productName","modelYear","listPrice", "category", "brand"})
public class Product implements Serializable, BeanT<Long> {

    @Getter
    @Column(name="product_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Getter
    @Setter
    @Column(name="PRODUCT_NAME",nullable=false)
    private String productName;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="BRAND_ID", nullable=false)
    private Brand brand;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="CATEGORY_ID", nullable=false)
    private Category category;

    @NonNull
    @Getter
    @Setter
    @Column(name="MODEL_YEAR")
    private Integer modelYear;

    @NonNull
    @Getter
    @Setter
    @Column(name="LIST_PRICE")
    private BigDecimal listPrice;

    @OneToMany (targetEntity=Stock.class, mappedBy="product" , cascade = CascadeType.REMOVE)
    private List<Stock> stocks = new ArrayList<>();

    @OneToMany (targetEntity=OrderItem.class, mappedBy="product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<OrderItem> orderItems = new HashSet<>();


}
