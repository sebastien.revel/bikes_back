package org.srevel.bikes.bean;

import lombok.*;
import org.srevel.bikes.bean.key.StockId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "stocks")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id", "quantity"})
@ToString(of= {"id", "quantity"})
public class Stock implements Serializable, BeanT<StockId> {

    @EmbeddedId
    @Getter
    @Setter
    private StockId id;


    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "STORE_ID", referencedColumnName = "STORE_ID", insertable = false, updatable = false)
//    @JoinColumn(name = "STORE_ID", referencedColumnName = "STORE_ID", insertable = false, updatable = false)
    private Store store;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", insertable = false, updatable = false)
    private Product product;

    @NonNull
    @Getter
    @Setter
    @Column(name="QUANTITY",nullable=false)
    private Integer quantity;

}
