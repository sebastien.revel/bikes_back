package org.srevel.bikes.bean;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "customers")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id"})
@ToString(of= {"id","firstName","lastName","phone","email","street","city","state","zipCode"})
public class Customer implements Serializable, BeanT<Long> {

    @Getter
    @Column(name="customer_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @NonNull
    @Column(name="FIRST_NAME")
    private String firstName;

    @Getter
    @Setter
    @NonNull
    @Column(name="LAST_NAME")
    private String lastName;


    @Getter
    @Setter
    @NonNull
    @Column(name="PHONE")
    private String phone;


    @Getter
    @Setter
    @NonNull
    @Column(name="EMAIL")
    private String email;

    @Getter
    @Setter
    @NonNull
    @Column(name="STREET")
    private String street;

    @Getter
    @Setter
    @NonNull
    @Column(name="CITY")
    private String city;

    @Getter
    @Setter
    @NonNull
    @Column(name="STATE")
    private String state;

    @Getter
    @Setter
    @NonNull
    @Column(name="ZIP_CODE")
    private String zipCode;

    @Getter
    @JsonManagedReference // break infinite loop
    @OneToMany (targetEntity=Order.class, mappedBy="customer" , fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<Order> orders  = new HashSet<Order>();

}
