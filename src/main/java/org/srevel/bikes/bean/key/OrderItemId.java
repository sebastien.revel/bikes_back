package org.srevel.bikes.bean.key;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@EqualsAndHashCode(of= {"orderId", "itemId"})
@Getter
@Setter
public class OrderItemId implements Serializable {

    @NonNull
    @Getter
    @Setter
    @Column(name="ORDER_ID",nullable=false)
    private Long orderId;

    @NonNull
    @Getter
    @Setter
    @Column(name="ITEM_ID",nullable=false)
    private Long itemId;


}

