package org.srevel.bikes.bean.key;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@EqualsAndHashCode(of= {"storeId", "productId"})
public class StockId implements Serializable {

    @Getter
    @Setter
//    @ManyToOne
    @Column(name="STORE_ID")
//    private Store store;
    private Long storeId;


    @Getter
    @Setter
    @Column(name="PRODUCT_ID",nullable=false)
    private Long productId;

}
