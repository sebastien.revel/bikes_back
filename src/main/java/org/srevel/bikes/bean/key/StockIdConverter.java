package org.srevel.bikes.bean.key;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StockIdConverter implements Converter<String, StockId> {

    @Override
    public StockId convert(String from) {
        String[] separated = from.split("-");
        return new StockId(Long.parseLong(separated[0]),Long.parseLong(separated[1]));
    }
}