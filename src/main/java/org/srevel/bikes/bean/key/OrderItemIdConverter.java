package org.srevel.bikes.bean.key;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class OrderItemIdConverter implements Converter<String, OrderItemId> {

    @Override
    public OrderItemId convert(String from) {
        String[] separated = from.split("-");
        return new OrderItemId(Long.parseLong(separated[0]),Long.parseLong(separated[1]));
    }
}