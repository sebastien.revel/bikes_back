package org.srevel.bikes.bean;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "stores")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id","storeName","phone","email","street","city","state","zipCode"})
@ToString(of= {"id","storeName","phone","email","street","city","state","zipCode"})
@JsonIgnoreProperties ({"orders","stocks", "staffs"})
public class Store implements Serializable, BeanT<Long> {

    @Getter
    @Column(name="STORE_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Getter
    @Setter
    @Column(name="STORE_NAME",nullable=false)
    private String storeName;

    @NonNull
    @Getter
    @Setter
    @Column(name="PHONE",nullable=false)
    private String phone;

    @NonNull
    @Getter
    @Setter
    @Column(name="EMAIL")
    private String email;

    @NonNull
    @Getter
    @Setter
    @Column(name="STREET")
    private String street;

    @NonNull
    @Getter
    @Setter
    @Column(name="CITY")
    private String city;

    @NonNull
    @Getter
    @Setter
    @Column(name="STATE")
    private String state;

    @NonNull
    @Getter
    @Setter
    @Column(name="ZIP_CODE")
    private String zipCode;

    @OneToMany (targetEntity=Order.class, mappedBy="store" , fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Getter
    private Set<Order> orders = new HashSet<>();

    @OneToMany (targetEntity=Stock.class, mappedBy="store", fetch = FetchType.LAZY , cascade = CascadeType.REMOVE)
    /*
    @MapsId("id")
    @OneToMany(targetEntity=Stock.class, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(table = "stocks", name = "STORE_ID", referencedColumnName = "STORE_ID", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT)),
            @JoinColumn(table = "stocks", name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    })
*/
    @Getter
    private Set<Stock> stocks = new HashSet<>();

    @OneToMany (targetEntity=Staff.class, mappedBy="store" , fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Getter
    private Set<Staff> staffs = new HashSet<>();

}
