package org.srevel.bikes.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id"})
@ToString(of= {"id","categoryName"})
@JsonIgnoreProperties({"products"})
public class Category implements Serializable , BeanT<Long> {

    @Getter
    @Column(name="CATEGORY_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Getter
    @Setter
    @Column(name="CATEGORY_NAME",nullable=false)
    private String categoryName;

    @OneToMany (targetEntity=Product.class, mappedBy="category", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE )
    @Getter
    private Set<Product> products = new HashSet<>();

}
