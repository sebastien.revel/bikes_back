package org.srevel.bikes.bean;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.srevel.bikes.bean.key.OrderItemId;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "order_items")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id","quantity","listPrice","discount"})
@ToString(of= {"id","quantity","listPrice","discount"})
public class OrderItem implements Serializable, BeanT <OrderItemId> {

    @EmbeddedId
    @Getter
    private OrderItemId id;

    @NonNull
    @JsonBackReference // break infinite loop
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ORDER_ID", insertable = false, updatable = false)
    private Order order;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
    private Product product;

    @NonNull
    @Getter
    @Setter
    @Column(name="QUANTITY")
    private Integer quantity;

    @NonNull
    @Getter
    @Setter
    @Column(name="LIST_PRICE")
    private BigDecimal listPrice; // Prix catalogue

    @NonNull
    @Getter
    @Setter
    @Column(name="DISCOUNT")
    private BigDecimal discount;

}
