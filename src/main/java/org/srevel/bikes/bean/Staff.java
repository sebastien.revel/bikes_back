package org.srevel.bikes.bean;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "staffs")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id"})
@ToString(of= {"id","firstName","lastName","email","phone","active","store", "manager"})
public class Staff implements Serializable, BeanT<Long> {

    @Getter
    @Column(name="staff_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Getter
    @Setter
    @Column(name="FIRST_NAME",nullable=false)
    private String firstName;

    @NonNull
    @Getter
    @Setter
    @Column(name="LAST_NAME",nullable=false)
    private String lastName;

    @NonNull
    @Getter
    @Setter
    @Column(name="EMAIL")
    private String email;

    @NonNull
    @Getter
    @Setter
    @Column(name="PHONE")
    private String phone;

    @NonNull
    @Getter
    @Setter
    @Column(name="ACTIVE")
    private Boolean active;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="STORE_ID", nullable=false)
    private Store store;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="MANAGER_ID")
    private Staff manager;

}
