package org.srevel.bikes.bean;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id","status","orderDate","requiredDate","shippedDate"})
@ToString(of= {"id","status","orderDate","requiredDate","shippedDate"})
public class Order implements Serializable, BeanT <Long> {

    @Getter
    @Column(name="order_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Getter
    @Setter
    @Column(name="ORDER_STATUS",nullable=false)
    // -- Order status: 1 = Pending; 2 = Processing; 3 = Rejected; 4 = Completed
    private Long status;

    @NonNull
    @Getter
    @Setter
    @Column(name="ORDER_DATE")
    private Date orderDate;

    @NonNull
    @Getter
    @Setter
    @Column(name="REQUIRED_DATE")
    private Date requiredDate;

    @Getter
    @Setter
    @Column(name="SHIPPED_DATE")
    private Date shippedDate;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="STORE_ID", nullable=false)
    private Store store;

    @NonNull
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="STAFF_ID", nullable=false)
    private Staff staff;

    @JsonBackReference // break infinite loop
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="CUSTOMER_ID")
    private Customer customer;

    @NonNull
    @JsonManagedReference // break infinite loop
    @OneToMany(mappedBy = "id.orderId", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Getter
    @Setter
    private Set<OrderItem> orderItems = new HashSet<>();

}
