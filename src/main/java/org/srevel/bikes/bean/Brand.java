package org.srevel.bikes.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "BRANDS")
@RequiredArgsConstructor(staticName="of")
@NoArgsConstructor
@EqualsAndHashCode(of= {"id"})
@ToString(of= {"id","brandName"})
@JsonIgnoreProperties({"products"})
public class Brand implements Serializable , BeanT<Long> {

    @Getter
    @Column(name="BRAND_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Getter
    @Setter
    @Column(name="BRAND_NAME",nullable=false)
    private String brandName;

    @OneToMany (targetEntity=Product.class, mappedBy="brand", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE )
    @Getter
    private Set<Product> products = new HashSet<>();


}
