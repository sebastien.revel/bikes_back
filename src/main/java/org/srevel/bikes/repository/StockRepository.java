package org.srevel.bikes.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Stock;
import org.srevel.bikes.bean.key.StockId;

import java.util.List;

@Repository
public interface StockRepository extends GenericRepository<Stock, StockId> {

    @Query("SELECT m FROM Stock m WHERE m.id.storeId=:storeId")
    List<Stock> searchByStoreId(@Param("storeId") Long storeId);

    @Query("SELECT m FROM Stock m WHERE m.id.productId=:productId")
    List<Stock> searchByProductId(@Param("productId") Long productId);


}
