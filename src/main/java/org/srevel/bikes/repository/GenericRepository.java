package org.srevel.bikes.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface GenericRepository <T, D>  extends Serializable ,  PagingAndSortingRepository <T, D> {

}
