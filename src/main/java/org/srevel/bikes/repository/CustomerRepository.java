package org.srevel.bikes.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Customer;

import java.util.List;

@Repository
public interface CustomerRepository extends GenericRepository<Customer, Long> {

    @Query("SELECT m FROM Customer m WHERE m.firstName LIKE %:pattern% OR  m.lastName LIKE %:pattern%")
    List<Customer> searchByFirstnameOrLastnameLike(@Param("pattern") String pattern);


}
