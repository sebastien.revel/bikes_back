package org.srevel.bikes.repository;

import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.OrderItem;
import org.srevel.bikes.bean.key.OrderItemId;

@Repository
public interface OrderItemRepository extends GenericRepository<OrderItem, OrderItemId> {

}
