package org.srevel.bikes.repository;

import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Brand;

@Repository
public interface BrandRepository extends GenericRepository<Brand, Long> {

}
