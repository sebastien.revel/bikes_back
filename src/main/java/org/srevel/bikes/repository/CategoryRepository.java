package org.srevel.bikes.repository;

import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Category;

@Repository
public interface CategoryRepository extends GenericRepository<Category, Long> {

}
