package org.srevel.bikes.repository;

import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order, Long> {

}
