package org.srevel.bikes.repository;

import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Store;

@Repository
public interface StoreRepository extends GenericRepository<Store, Long> {

}
