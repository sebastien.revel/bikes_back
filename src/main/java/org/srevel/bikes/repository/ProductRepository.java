package org.srevel.bikes.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Product;

import java.util.List;

@Repository
public interface ProductRepository extends GenericRepository<Product, Long> {


    @Query("SELECT m FROM Product m WHERE m.productName LIKE %:pattern%")
    List<Product> searchByNameLike(@Param("pattern") String pattern);


}
