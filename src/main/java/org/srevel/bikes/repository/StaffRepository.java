package org.srevel.bikes.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.srevel.bikes.bean.Staff;

import java.util.List;

@Repository
public interface StaffRepository extends GenericRepository<Staff, Long> {

    @Query("SELECT m FROM Staff m WHERE m.firstName LIKE %:pattern% OR m.lastName LIKE %:pattern%")
    List<Staff> searchByFirstnameOrLastnameLike(@Param("pattern") String pattern);


}
