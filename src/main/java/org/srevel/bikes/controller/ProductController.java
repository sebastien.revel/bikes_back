package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.srevel.bikes.bean.Product;
import org.srevel.bikes.service.GenericService;
import org.srevel.bikes.service.ProductService;

@RestController
@RequestMapping("products")
@Slf4j(topic = "ProductController")
public class ProductController extends GenericController <Product, Long> {
    @Autowired
    ProductService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Product, Long> getService() {
        return service;
    }


    @GetMapping(value = "/like/{pattern}", produces = "application/json")
    public Iterable<Product> searchByNameLike(@PathVariable @NotNull String pattern) {
        getLog().info("searchByNameLike pattern :: " + pattern);
        return service.searchByNameLike(pattern);
    }
}
