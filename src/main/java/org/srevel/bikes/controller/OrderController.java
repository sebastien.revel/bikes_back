package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.srevel.bikes.bean.Order;
import org.srevel.bikes.service.GenericService;
import org.srevel.bikes.service.OrderService;

@RestController
@RequestMapping("orders")
@Slf4j(topic = "OrderController")
public class OrderController extends GenericController <Order, Long> {
    @Autowired
    OrderService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Order, Long> getService() {
        return service;
    }

}
