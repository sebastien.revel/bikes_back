package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.srevel.bikes.bean.OrderItem;
import org.srevel.bikes.bean.key.OrderItemId;
import org.srevel.bikes.service.GenericService;
import org.srevel.bikes.service.OrderItemService;

@RestController
@RequestMapping("orderitems")
@Slf4j(topic = "OrderItemController")
public class OrderItemController extends GenericController <OrderItem, OrderItemId> {
    @Autowired
    OrderItemService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<OrderItem, OrderItemId> getService() {
        return service;
    }

    @PostMapping(value= "", produces = "application/json")
    public OrderItem create(@RequestBody OrderItem newEntity) {
        getLog().info("create :: " + newEntity);
        return getService().save(newEntity);
    }


    // Exemple :: GET http://localhost:8080/orders/order/3/item/2
    @GetMapping(value = "/order/{orderId}/item/{itemId}", produces = "application/json")
    public OrderItem getById  (@PathVariable @NotNull Long orderId, @PathVariable @NotNull Long itemId) {
        getLog().info("getById orderId :: " + orderId+", itemId :: "+itemId);
        OrderItemId orderItemId = new OrderItemId(orderId, itemId);
        if (getService().existsById(orderItemId)) {
            return getService().findById(orderItemId).get();
        }
        else {
            getLog().error ("OrderItem with id ("+orderId+", "+itemId+") is not found !");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
        }
    }
}
