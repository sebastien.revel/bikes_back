package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.srevel.bikes.bean.Staff;
import org.srevel.bikes.service.GenericService;
import org.srevel.bikes.service.StaffService;

@RestController
@RequestMapping("staffs")
@Slf4j(topic = "StaffController")
public class StaffController extends GenericController <Staff, Long> {
    @Autowired
    StaffService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Staff, Long> getService() {
        return service;
    }


    @GetMapping(value = "/like/{pattern}", produces = "application/json")
    public Iterable<Staff> searchByFirstnameOrLastnameLike(@PathVariable @NotNull String pattern) {
        getLog().info("searchByFirstnameOrLastnameLike pattern :: " + pattern);
        return service.searchByFirstnameOrLastnameLike(pattern);
    }
}
