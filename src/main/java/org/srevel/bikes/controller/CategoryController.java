package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.srevel.bikes.bean.Category;
import org.srevel.bikes.bean.Product;
import org.srevel.bikes.service.CategoryService;
import org.srevel.bikes.service.GenericService;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("categories")
@Slf4j(topic = "CategoryController")
public class CategoryController extends GenericController <Category, Long> {
    @Autowired
    CategoryService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Category, Long> getService() {
        return service;
    }

    @GetMapping(value = "/{id}/products", produces = "application/json")
    public Set<Product> getProducts(@PathVariable @NotNull Long id) {
        getLog().info("getProducts id :: " + id);
        Optional<Category> category = getService().findById(id);
        if (! category.isEmpty()) {
            getLog().info("getProducts category :: " + category.get());
            return category.get().getProducts();
        }

        getLog().error ("Category with id ("+id+") is not found !");
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
    }
}
