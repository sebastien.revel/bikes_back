package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.srevel.bikes.bean.Customer;
import org.srevel.bikes.bean.Order;
import org.srevel.bikes.service.CustomerService;
import org.srevel.bikes.service.GenericService;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("customers")
@Slf4j(topic = "CustomerController")
public class CustomerController  extends GenericController <Customer, Long> {
    @Autowired
    CustomerService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Customer, Long> getService() {
        return service;
    }

    @GetMapping(value = "/like/{pattern}", produces = "application/json")
    public Iterable<Customer> searchByFirstnameOrLastnameLike(@PathVariable @NotNull String pattern) {
        getLog().info("searchByFirstnameOrLastnameLike pattern :: " + pattern);
        return service.searchByFirstnameOrLastnameLike(pattern);
    }

    @GetMapping(value = "/{id}/orders", produces = "application/json")
    public Set<Order> getOrders(@PathVariable @NotNull Long id) {
        getLog().info("getOrders id :: " + id);
        Optional<Customer> cust = getService().findById(id);
        if (! cust.isEmpty()) {
            getLog().info("getOrders cust :: " + cust.get());
            return cust.get().getOrders();
        }

        getLog().error ("Entity with id ("+id+") is not found !");
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
    }
}
