package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.srevel.bikes.bean.Stock;
import org.srevel.bikes.bean.key.StockId;
import org.srevel.bikes.service.GenericService;
import org.srevel.bikes.service.StockService;

@RestController
@RequestMapping("stocks")
@Slf4j(topic = "StockController")
public class StockController extends GenericController <Stock, StockId> {
    @Autowired
    StockService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Stock, StockId> getService() {
        return service;
    }

    @PostMapping(value= "", produces = "application/json")
    public Stock create(@RequestBody Stock newEntity) {
        getLog().info("create :: " + newEntity);
        return getService().save(newEntity);
    }

    // Exemple :: GET http://localhost:8080/stocks/store/3/product/63
    @GetMapping(value = "/store/{storeId}/product/{productId}", produces = "application/json")
    public Stock getById  (@PathVariable @NotNull Long storeId, @PathVariable @NotNull Long productId) {
        getLog().info("getById storeId :: " + storeId+", productId :: "+productId);
        StockId stockId = new StockId(storeId, productId);
        if (getService().existsById(stockId)) {
            return getService().findById(stockId).get();
        }
        else {
            getLog().error ("Stock with id ("+storeId+", "+productId+") is not found !");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
        }
    }

    // Exemple :: GET http://localhost:8080/stocks/store/3
    @GetMapping(value = "/store/{storeId}", produces = "application/json")
    public Iterable<Stock>  getByStoreId  (@PathVariable @NotNull Long storeId) {
        getLog().info("getByStoreId storeId :: " + storeId);
        return service.searchByStoreId(storeId);
    }

    // Exemple :: GET http://localhost:8080/stocks/product/3
    @GetMapping(value = "/product/{productId}", produces = "application/json")
    public Iterable<Stock>  getByProductId  (@PathVariable @NotNull Long productId) {
        getLog().info("getByProductId productId :: " + productId);
        return service.searchByProductId(productId);
    }
}
