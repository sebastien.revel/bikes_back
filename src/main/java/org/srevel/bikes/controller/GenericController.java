package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.srevel.bikes.bean.BeanT;
import org.srevel.bikes.service.GenericService;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(method={RequestMethod.POST,RequestMethod.GET,RequestMethod.DELETE,RequestMethod.PUT})
public abstract class GenericController <T, D>  {

    abstract GenericService<T, D> getService() ;
    abstract Logger getLog() ;

    @GetMapping(value = "/count", produces = "application/json")
    public long count  () {
        getLog().info("count");
        return  getService().count();
    }


    // Exemple :: GET http://localhost:8080/customers/2
    @GetMapping(value = "/{id}", produces = "application/json")
    public T getById  (@PathVariable @NotNull  D id) {
        getLog().info("getById id :: " + id);
        if (getService().existsById(id)) {
            return (T) getService().findById(id);
        }
        else {
            getLog().error ("Entity with id ("+id+") is not found !");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
        }
    }

    // Exemple :: GET http://localhost:8080/orderitems/composite/62-3
    @GetMapping(value = "/composite/{id}", produces = "application/json")
    public T getByCompositeId  (@PathVariable @NotNull  D id) {
        getLog().info("getById id :: " + id);
        if (getService().existsById(id)) {
            return (T) getService().findById(id);
        }
        else {
            getLog().error ("Entity with id ("+id+") is not found !");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
        }
    }

    // Exemple :: GET http://localhost:8080/customers/2,4,56
    @GetMapping(value = "/many/{ids}", produces = "application/json")
    public Iterable<T> getAllByIds  (@PathVariable @NotNull  Set<D> ids) {
        getLog().info("getAllByIds ids :: " + ids);
        List result = new ArrayList<T>();
        for(D unique_id : ids){
            getLog().info("get unique_id :: " + unique_id);
            if (getService().existsById(unique_id)) {
                result.add(getService().findById(unique_id));
            }
            else {
                getLog().error ("Entity with id ("+unique_id+") is not found !");
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
            }
        }

        return result;
    }

    // Exemple :: http://localhost:8080/customers?page=10&size=20&sort=firstName,asc&sort=lastName,asc
    @GetMapping("")
    @ResponseBody
    public List<T> getAll  (
                        @RequestParam(name = "page", required = false, defaultValue  = "0") Integer page ,
                        @RequestParam(name = "size", required = false) Integer size,
                        @RequestParam(name = "sort", required = false) String[] sort) {

        getLog().info("findAllPageableAndSort :: "+page+", "+size+", "+sort);

        List<Order> orders = new ArrayList<>();

        if (sort != null && sort.length>0) {
            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }
        }

        return  getService().getAll (page, size, orders);
    }

    private Sort.Direction getSortDirection(String s) {
        return s.equals("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;
    }



    @DeleteMapping(value = "", produces = "application/json")
    public void deleteAll  () {
        getLog().info("deleteAll !! ");
        getService().deleteAll();
    }


    // Exemple :: DELETE http://localhost:8080/customers/2
    // Exemple :: DELETE http://localhost:8080/customers/2,4,56
    @DeleteMapping(value = "/{ids}", produces = "application/json")
    public void deleteAllByIds  (@PathVariable @NotNull  Set<D> ids) {
        getLog().info("deleteAllByIds ids :: " + ids);
        for(D unique_id : ids){
            if (!getService().existsById(unique_id)) {
                getLog().error ("Entity with id ("+unique_id+") is not found !");
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
            }
        }
        for(D unique_id : ids){
            getLog().info("delete unique_id :: " + unique_id);
            getService().deleteById(unique_id);
        }


    }

    @PutMapping(value= "/{id}", produces = "application/json")
    public T update (@PathVariable @NotNull  D id, @RequestBody T newEntity) {
        getLog().info("update :: " + id+", "+newEntity);
        if (((BeanT)newEntity).getId() == null) {
            getLog().error ("Entity id can't be empty !");
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Entity id can't be empty !");
        }
        if (! ((BeanT)newEntity).getId().equals(id)) {
            getLog().error ("Entity id must be equals to id ("+id+")!");
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Entity id must be equals to id ("+id+")!");
        }

        if (getService().existsById(id)) {
            T origin = getService().findById(id).get();
            if (origin != null) {
                try {
                    BeanUtils.copyProperties(newEntity, origin, getNullProperties (newEntity));
                } catch (Exception e) {
                    getLog().error ("error");
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "error");
                }
                return getService().save(origin);
            }
        }
        else {
            getLog().error ("Entity with id ("+id+") is not found !");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
        }

        getLog().info("return null :(");
        return null;
    }

    private String[] getNullProperties (Object bean) throws InvocationTargetException, IllegalAccessException {
        PropertyDescriptor[] properties = BeanUtils.getPropertyDescriptors( bean.getClass());
        List <String> nullProperties = new ArrayList();
        for (PropertyDescriptor prop : properties){
            Object readValue = prop.getReadMethod().invoke(bean);
            if (readValue == null && ! prop.getName().equals("class")) {
                nullProperties.add(prop.getName());
            }
        }
        return nullProperties.toArray(new String[0]);
    }

    @PostMapping(value= "", produces = "application/json")
    public T create(@RequestBody T newEntity) {
        getLog().info("create :: " + newEntity);
        if (((BeanT)newEntity).getId() != null) {
            getLog().error ("Entity can't be created because id is not null !");
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Entity can't be created because id is not null !");
        }
        else {
            return getService().save(newEntity);
        }
    }

}
