package org.srevel.bikes.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.srevel.bikes.bean.Order;
import org.srevel.bikes.bean.Staff;
import org.srevel.bikes.bean.Stock;
import org.srevel.bikes.bean.Store;
import org.srevel.bikes.service.GenericService;
import org.srevel.bikes.service.StoreService;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("stores")
@Slf4j(topic = "StoreController")
public class StoreController extends GenericController <Store, Long> {
    @Autowired
    StoreService service ;

    @Override
    Logger getLog() {
        return log;
    }

    @Override
    GenericService<Store, Long> getService() {
        return service;
    }


    @GetMapping(value = "/{id}/staffs", produces = "application/json")
    public Set<Staff> getStaffs(@PathVariable @NotNull Long id) {
        getLog().info("getStaffs id :: " + id);
        Optional<Store> store = getService().findById(id);
        if (! store.isEmpty()) {
            getLog().info("getStaffs store :: " + store.get());
            return store.get().getStaffs();
        }

        getLog().error ("Order with id ("+id+") is not found !");
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
    }


    @GetMapping(value = "/{id}/stocks", produces = "application/json")
    public Set<Stock> getStocks(@PathVariable @NotNull Long id) {
        getLog().info("getStocks id :: " + id);
        Optional<Store> store = getService().findById(id);
        if (! store.isEmpty()) {
            getLog().info("getStocks store :: " + store.get()) ;
            return store.get().getStocks();
        }

        getLog().error ("Stock with id ("+id+") is not found !");
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
    }


    @GetMapping(value = "/{id}/orders", produces = "application/json")
    public Set<Order> getOrders(@PathVariable @NotNull Long id) {
        getLog().info("getOrders id :: " + id);
        Optional<Store> store = getService().findById(id);
        if (! store.isEmpty()) {
            getLog().info("getOrders store :: " + store.get());
            return store.get().getOrders();
        }

        getLog().error ("Stock with id ("+id+") is not found !");
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This element doesn't exist");
    }
}
