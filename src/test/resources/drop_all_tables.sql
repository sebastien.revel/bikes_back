
-- drop tables
DROP TABLE order_items CASCADE;
DROP TABLE orders CASCADE;
DROP TABLE stocks CASCADE;
DROP TABLE products CASCADE;
DROP TABLE categories CASCADE;
DROP TABLE brands CASCADE;
DROP TABLE customers CASCADE;
DROP TABLE staffs CASCADE;
DROP TABLE stores CASCADE;

-- drop the schemas

--DROP SCHEMA IF EXISTS sales;
--DROP SCHEMA IF EXISTS production;



