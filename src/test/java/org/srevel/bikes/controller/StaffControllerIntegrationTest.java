
package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Staff Controller Test")
@Slf4j(topic = "StaffControllerIntegrationTest")
public class StaffControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "staffs";
    int initialNb = 10;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(1)))
                    .andExpect(jsonPath("$.firstName", is("Fabiola")))
                    .andExpect(jsonPath("$.lastName", is("Jackson")))
                    .andExpect(jsonPath("$.email", is("fabiola.jackson@bikes.shop")))
                    .andExpect(jsonPath("$.phone", is("(831) 555-5554")))
                    .andExpect(jsonPath("$.active", is(true)))
                    .andExpect(jsonPath("$.manager", is(IsNull.nullValue())))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$.store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$.store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$.store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$.store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$.store.state", is("CA")))
                    .andExpect(jsonPath("$.store.zipCode", is("95060")))
                ;
        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1,2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(2)))

                    .andExpect(jsonPath("$[0].id", is(1)))
                    .andExpect(jsonPath("$[0].firstName", is("Fabiola")))
                    .andExpect(jsonPath("$[0].lastName", is("Jackson")))
                    .andExpect(jsonPath("$[0].email", is("fabiola.jackson@bikes.shop")))
                    .andExpect(jsonPath("$[0].phone", is("(831) 555-5554")))
                    .andExpect(jsonPath("$[0].active", is(true)))
                    .andExpect(jsonPath("$[0].manager", is(IsNull.nullValue())))

                    .andExpect(jsonPath("$[0].store.id", is(1)))
                    .andExpect(jsonPath("$[0].store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$[0].store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$[0].store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$[0].store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$[0].store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$[0].store.state", is("CA")))
                    .andExpect(jsonPath("$[0].store.zipCode", is("95060")))


                    .andExpect(jsonPath("$[1].id", is(2)))
                    .andExpect(jsonPath("$[1].firstName", is("Mireya")))
                    .andExpect(jsonPath("$[1].lastName", is("Copeland")))
                    .andExpect(jsonPath("$[1].email", is("mireya.copeland@bikes.shop")))
                    .andExpect(jsonPath("$[1].phone", is("(831) 555-5555")))
                    .andExpect(jsonPath("$[1].active", is(true)))
                    .andExpect(jsonPath("$[1].manager", is(IsNull.notNullValue())))

                    .andExpect(jsonPath("$[1].store.id", is(1)))
                    .andExpect(jsonPath("$[1].store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$[1].store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$[1].store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$[1].store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$[1].store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$[1].store.state", is("CA")))
                    .andExpect(jsonPath("$[1].store.zipCode", is("95060")))
                ;
        }


        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1,55")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {


        @Test
        @DisplayName("Create With Id")
        public void givenInitialization_whenCreateWithId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "id":1555,
                                                "firstName": "Sébastien",
                                                "lastName": "REVEL",
                                                "email": "s.revel@bikes.shop",
                                                "phone": "(516) 379-4445",
                                                "active": true,
                                                "manager": {
                                                    "id": 1
                                                },
                                                "store": {
                                                    "id": 1
                                                }
                                            }
                                            """)
                    )
                    .andExpect(status().isConflict())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "firstName": "Sébastien",
                                                "lastName": "REVEL",
                                                "email": "s.revel@bikes.shop",
                                                "phone": "(516) 379-4445",
                                                "active": true,
                                                "manager": {
                                                    "id": 1
                                                },
                                                "store": {
                                                    "id": 1
                                                }
                                            }
                                            """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(initialNb+1)))
                    .andExpect(jsonPath("$.firstName", is("Sébastien")))
                    .andExpect(jsonPath("$.lastName", is("REVEL")))
                    .andExpect(jsonPath("$.email", is("s.revel@bikes.shop")))
                    .andExpect(jsonPath("$.phone", is("(516) 379-4445")))
                    .andExpect(jsonPath("$.active", is(true)))
                    .andExpect(jsonPath("$.manager", is(IsNull.notNullValue())))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 5);
            assertExist(entityName, 6);

            // Need to delete all orders to avoid foreign key violation
            mvc.perform(
                            delete("/orders")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk());

            mvc.perform(
                            delete("/"+entityName+"/6,7")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-2);
            assertNotExist(entityName, 6);
            assertNotExist(entityName, 7);
        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2,4000,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 6);

            // Need to delete all orders to avoid foreign key violation
            mvc.perform(
                            delete("/orders")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk());

            mvc.perform(
                            delete("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);
            assertNotExist(entityName, 6);

        }

        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            // Need to delete all orders to avoid foreign key violation
            mvc.perform(
                            delete("/orders")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk());

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "firstName": "Sébastien",
                                                "lastName": "REVEL",
                                                "email": "s.revel@bikes.shop",
                                                "phone": "(516) 379-4445",
                                                "active": true,
                                                "manager": {
                                                    "id": 1
                                                },
                                                "store": {
                                                    "id": 1
                                                }
                                            }
                                            """))
                    .andExpect(status().isConflict());

            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "id":1000,
                                                "firstName": "Sébastien",
                                                "lastName": "REVEL",
                                                "email": "s.revel@bikes.shop",
                                                "phone": "(516) 379-4445",
                                                "active": true,
                                                "manager": {
                                                    "id": 1
                                                },
                                                "store": {
                                                    "id": 1
                                                }                                        
                                            }
                                            """))
                    .andExpect(status().isNotFound());

            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                    get("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(1)))
                    .andExpect(jsonPath("$.firstName", is("Fabiola")))
                    .andExpect(jsonPath("$.lastName", is("Jackson")))
                    .andExpect(jsonPath("$.email", is("fabiola.jackson@bikes.shop")))
                    .andExpect(jsonPath("$.phone", is("(831) 555-5554")))
                    .andExpect(jsonPath("$.active", is(true)))
                    .andExpect(jsonPath("$.manager", is(IsNull.nullValue())))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$.store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$.store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$.store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$.store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$.store.state", is("CA")))
                    .andExpect(jsonPath("$.store.zipCode", is("95060")))
                ;

            mvc.perform(
                            put("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":"1",
                                            "firstName":"Sébastien"
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(1)))
                    .andExpect(jsonPath("$.firstName", is("Sébastien")))
                    .andExpect(jsonPath("$.lastName", is("Jackson")))
                    .andExpect(jsonPath("$.email", is("fabiola.jackson@bikes.shop")))
                    .andExpect(jsonPath("$.phone", is("(831) 555-5554")))
                    .andExpect(jsonPath("$.active", is(true)))
                    .andExpect(jsonPath("$.manager", is(IsNull.nullValue())))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$.store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$.store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$.store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$.store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$.store.state", is("CA")))
                    .andExpect(jsonPath("$.store.zipCode", is("95060")))
            ;
        }
    }

}
