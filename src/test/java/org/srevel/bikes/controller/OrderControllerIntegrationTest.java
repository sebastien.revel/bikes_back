
package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DisplayName("Order Controller Test")
@Slf4j(topic = "OrderControllerIntegrationTest")
public class OrderControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "orders";
    int initialNb = 1615;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(1)))
                    .andExpect(jsonPath("$.status", is(4)))
                    .andExpect(jsonPath("$.orderDate", is("2016-01-01")))
                    .andExpect(jsonPath("$.requiredDate", is("2016-01-03")))
                    .andExpect(jsonPath("$.shippedDate", is("2016-01-03")))
                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.staff.id", is(2)))
                    .andExpect(jsonPath("$.orderItems", IsCollectionWithSize.hasSize(5)))
            ;


        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1,3")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(2)))

                    .andExpect(jsonPath("$[0].id", is(1)))
                    .andExpect(jsonPath("$[0].status", is(4)))
                    .andExpect(jsonPath("$[0].orderDate", is("2016-01-01")))
                    .andExpect(jsonPath("$[0].requiredDate", is("2016-01-03")))
                    .andExpect(jsonPath("$[0].shippedDate", is("2016-01-03")))
                    .andExpect(jsonPath("$[0].store.id", is(1)))
                    .andExpect(jsonPath("$[0].staff.id", is(2)))
                    .andExpect(jsonPath("$[0].orderItems", IsCollectionWithSize.hasSize(5)))

                    .andExpect(jsonPath("$[1].id", is(3)))
                    .andExpect(jsonPath("$[1].status", is(4)))
                    .andExpect(jsonPath("$[1].orderDate", is("2016-01-02")))
                    .andExpect(jsonPath("$[1].requiredDate", is("2016-01-05")))
                    .andExpect(jsonPath("$[1].shippedDate", is("2016-01-03")))
                    .andExpect(jsonPath("$[1].store.id", is(2)))
                    .andExpect(jsonPath("$[1].staff.id", is(7)))
                    .andExpect(jsonPath("$[1].orderItems", IsCollectionWithSize.hasSize(2)))

                ;
        }



        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/10,2000,55")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {


        @Test
        @DisplayName("Create With Id")
        public void givenInitialization_whenCreateWithId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":2000,
                                            "status": 4,
                                            "orderDate": "2016-01-03",
                                            "requiredDate": "2016-01-06",
                                            "shippedDate": "2016-01-06"
                                            }
                                            """)
                    )
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                                {
                                                    "status": 4,
                                                    "orderDate": "2016-01-03",
                                                    "requiredDate": "2016-01-06",
                                                    "shippedDate": "2016-01-06",
                                                    "customer": {"id":1},
                                                    "store": {"id":1},
                                                    "staff": {"id":1}
                                                }
                                            """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(initialNb+1)))

                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 2);
            assertExist(entityName, 4);
            assertExist(entityName, 6);

            mvc.perform(
                            delete("/"+entityName+"/2,4,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-3);
            assertNotExist(entityName, 2);
            assertNotExist(entityName, 4);
            assertNotExist(entityName, 6);
        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2,4000,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 5);

            mvc.perform(
                            delete("/"+entityName+"/5")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);

            assertNotExist(entityName, 5);

        }

        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "status": 4,
                                            "orderDate": "2016-01-03",
                                            "requiredDate": "2016-01-06",
                                            "shippedDate": "2016-01-06",
                                            "customer": {"id":1},
                                            "store": {"id":1},
                                            "staff": {"id":1}
                                            }
                                            """))
                    .andExpect(status().isConflict());

            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/10000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":10000,
                                            "status": 4,
                                            "orderDate": "2016-01-03",
                                            "requiredDate": "2016-01-06",
                                            "shippedDate": "2016-01-06",
                                            "customer": {"id":1},
                                            "store": {"id":1},
                                            "staff": {"id":1}
                                            }
                                            """))
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                    get("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(1)))
                    .andExpect(jsonPath("$.status", is(4)))
                    .andExpect(jsonPath("$.orderDate", is("2016-01-01")))
                    .andExpect(jsonPath("$.requiredDate", is("2016-01-03")))
                    .andExpect(jsonPath("$.shippedDate", is("2016-01-03")))
                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.staff.id", is(2)))
                    .andExpect(jsonPath("$.orderItems", IsCollectionWithSize.hasSize(5)))
                ;

            mvc.perform(
                            put("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id": 1,
                                            "status": 3,
                                            "store": {
                                                "id": 2
                                            },
                                            "staff": {
                                                "id": 3
                                            }
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);


            mvc.perform(
                            get("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(1)))
                    .andExpect(jsonPath("$.status", is(3)))
                    .andExpect(jsonPath("$.orderDate", is("2016-01-01")))
                    .andExpect(jsonPath("$.requiredDate", is("2016-01-03")))
                    .andExpect(jsonPath("$.shippedDate", is("2016-01-03")))
                    .andExpect(jsonPath("$.store.id", is(2)))
                    .andExpect(jsonPath("$.staff.id", is(3)))
                    .andExpect(jsonPath("$.orderItems", IsCollectionWithSize.hasSize(5)))
            ;
        }
    }

}
