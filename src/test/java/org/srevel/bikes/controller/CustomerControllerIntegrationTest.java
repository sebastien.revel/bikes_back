package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Customer Controller Test")
@Slf4j(topic = "CustomerControllerIntegrationTest")
public class CustomerControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "customers";
    int initialNb = 1445;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(6)))
                    .andExpect(jsonPath("$.firstName", is("Lyndsey")))
                    .andExpect(jsonPath("$.lastName", is("Bean")))
                    .andExpect(jsonPath("$.phone", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$.email", is("lyndsey.bean@hotmail.com")))
                    .andExpect(jsonPath("$.street", is("769 West Road ")))
                    .andExpect(jsonPath("$.city", is("Fairport")))
                    .andExpect(jsonPath("$.state", is("NY")))
                    .andExpect(jsonPath("$.zipCode", is("14450")))
                    .andExpect(jsonPath("$.orders", IsCollectionWithSize.hasSize(3)))
                    .andExpect(jsonPath("$.orders[0].id", is(1059)))
                    .andExpect(jsonPath("$.orders[0].status", is(4)))
                    .andExpect(jsonPath("$.orders[0].orderDate", is("2017-08-14")))
                    .andExpect(jsonPath("$.orders[0].requiredDate", is("2017-08-17")))
                    .andExpect(jsonPath("$.orders[0].shippedDate", is("2017-08-17")))
                    .andExpect(jsonPath("$.orders[1].id", is(1611)))
                    .andExpect(jsonPath("$.orders[1].status", is(3)))
                    .andExpect(jsonPath("$.orders[1].orderDate", is("2018-09-06")))
                    .andExpect(jsonPath("$.orders[1].requiredDate", is("2018-09-06")))
                    .andExpect(jsonPath("$.orders[1].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$.orders[2].id", is(1592)))
                    .andExpect(jsonPath("$.orders[2].status", is(2)))
                    .andExpect(jsonPath("$.orders[2].orderDate", is("2018-04-27")))
                    .andExpect(jsonPath("$.orders[2].requiredDate", is("2018-04-27")))
                    .andExpect(jsonPath("$.orders[2].shippedDate", is(IsNull.nullValue())))
                ;
        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().is4xxClientError());
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/6,11,55")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(3)))

                    .andExpect(jsonPath("$[0].id", is(6)))
                    .andExpect(jsonPath("$[0].firstName", is("Lyndsey")))
                    .andExpect(jsonPath("$[0].lastName", is("Bean")))
                    .andExpect(jsonPath("$[0].phone", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[0].email", is("lyndsey.bean@hotmail.com")))
                    .andExpect(jsonPath("$[0].street", is("769 West Road ")))
                    .andExpect(jsonPath("$[0].city", is("Fairport")))
                    .andExpect(jsonPath("$[0].state", is("NY")))
                    .andExpect(jsonPath("$[0].zipCode", is("14450")))
                    .andExpect(jsonPath("$[0].orders", IsCollectionWithSize.hasSize(3)))
                    .andExpect(jsonPath("$[0].orders[0].id", is(1059)))
                    .andExpect(jsonPath("$[0].orders[0].status", is(4)))
                    .andExpect(jsonPath("$[0].orders[0].orderDate", is("2017-08-14")))
                    .andExpect(jsonPath("$[0].orders[0].requiredDate", is("2017-08-17")))
                    .andExpect(jsonPath("$[0].orders[0].shippedDate", is("2017-08-17")))
                    .andExpect(jsonPath("$[0].orders[1].id", is(1611)))
                    .andExpect(jsonPath("$[0].orders[1].status", is(3)))
                    .andExpect(jsonPath("$[0].orders[1].orderDate", is("2018-09-06")))
                    .andExpect(jsonPath("$[0].orders[1].requiredDate", is("2018-09-06")))
                    .andExpect(jsonPath("$[0].orders[1].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[0].orders[2].id", is(1592)))
                    .andExpect(jsonPath("$[0].orders[2].status", is(2)))
                    .andExpect(jsonPath("$[0].orders[2].orderDate", is("2018-04-27")))
                    .andExpect(jsonPath("$[0].orders[2].requiredDate", is("2018-04-27")))
                    .andExpect(jsonPath("$[0].orders[2].shippedDate", is(IsNull.nullValue())))

                    .andExpect(jsonPath("$[1].id", is(11)))
                    .andExpect(jsonPath("$[1].firstName", is("Deshawn")))
                    .andExpect(jsonPath("$[1].lastName", is("Mendoza")))
                    .andExpect(jsonPath("$[1].phone", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[1].email", is("deshawn.mendoza@yahoo.com")))
                    .andExpect(jsonPath("$[1].street", is("8790 Cobblestone Street ")))
                    .andExpect(jsonPath("$[1].city", is("Monsey")))
                    .andExpect(jsonPath("$[1].state", is("NY")))
                    .andExpect(jsonPath("$[1].zipCode", is("10952")))
                    .andExpect(jsonPath("$[1].orders", IsCollectionWithSize.hasSize(3)))
                    .andExpect(jsonPath("$[1].orders[0].id", is(1588)))
                    .andExpect(jsonPath("$[1].orders[0].status", is(1)))
                    .andExpect(jsonPath("$[1].orders[0].orderDate", is("2018-04-26")))
                    .andExpect(jsonPath("$[1].orders[0].requiredDate", is("2018-04-26")))
                    .andExpect(jsonPath("$[1].orders[0].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[1].orders[1].id", is(1074)))
                    .andExpect(jsonPath("$[1].orders[1].status", is(3)))
                    .andExpect(jsonPath("$[1].orders[1].orderDate", is("2017-08-19")))
                    .andExpect(jsonPath("$[1].orders[1].requiredDate", is("2017-08-19")))
                    .andExpect(jsonPath("$[1].orders[1].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[1].orders[2].id", is(1387)))
                    .andExpect(jsonPath("$[1].orders[2].status", is(4)))
                    .andExpect(jsonPath("$[1].orders[2].orderDate", is("2018-02-09")))
                    .andExpect(jsonPath("$[1].orders[2].requiredDate", is("2018-02-11")))
                    .andExpect(jsonPath("$[1].orders[2].shippedDate", is("2018-02-11")))


                    .andExpect(jsonPath("$[2].id", is(55)))
                    .andExpect(jsonPath("$[2].firstName", is("Diana")))
                    .andExpect(jsonPath("$[2].lastName", is("Guerra")))
                    .andExpect(jsonPath("$[2].phone", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[2].email", is("diana.guerra@hotmail.com")))
                    .andExpect(jsonPath("$[2].street", is("45 Chapel Ave. ")))
                    .andExpect(jsonPath("$[2].city", is("Merrick")))
                    .andExpect(jsonPath("$[2].state", is("NY")))
                    .andExpect(jsonPath("$[2].zipCode", is("11566")))
                    .andExpect(jsonPath("$[2].orders", IsCollectionWithSize.hasSize(2)))
                    .andExpect(jsonPath("$[2].orders[0].id", is(1602)))
                    .andExpect(jsonPath("$[2].orders[0].status", is(1)))
                    .andExpect(jsonPath("$[2].orders[0].orderDate", is("2018-04-30")))
                    .andExpect(jsonPath("$[2].orders[0].requiredDate", is("2018-04-30")))
                    .andExpect(jsonPath("$[2].orders[0].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$[2].orders[1].id", is(147)))
                    .andExpect(jsonPath("$[2].orders[1].status", is(4)))
                    .andExpect(jsonPath("$[2].orders[1].orderDate", is("2016-03-28")))
                    .andExpect(jsonPath("$[2].orders[1].requiredDate", is("2016-03-31")))
                    .andExpect(jsonPath("$[2].orders[1].shippedDate", is("2016-03-31")))
                ;
        }



        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/10,2000,55")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().is4xxClientError());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {


        @Test
        @DisplayName("Create With Id")
        public void givenInitialization_whenCreateWithId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":"1448",
                                            "firstName":"Fleur",
                                            "lastName":"REVEL",
                                            "phone":"66666666",
                                            "email":"sebastien.revel@gmail.com",
                                            "street":"rue de paris",
                                            "city":"Melun",
                                            "state":"FR",
                                            "zipCode":"77000"
                                            }
                                            """)
                    )
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                                {"firstName":"Sébastien",
                                                "lastName":"REVEL",
                                                "phone":"0102030405",
                                                "email":"s.r@yahoo.fr",
                                                "street":"rue de Paris",
                                                "city":"Melun",
                                                "state":"FR",
                                                "zipCode":"77000"}
                                         """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(initialNb+1)))
                    .andExpect(jsonPath("$.firstName", is("Sébastien")))
                    .andExpect(jsonPath("$.lastName", is("REVEL")))
                    .andExpect(jsonPath("$.phone", is("0102030405")))
                    .andExpect(jsonPath("$.email", is("s.r@yahoo.fr")))
                    .andExpect(jsonPath("$.street", is("rue de Paris")))
                    .andExpect(jsonPath("$.city", is("Melun")))
                    .andExpect(jsonPath("$.state", is("FR")))
                    .andExpect(jsonPath("$.zipCode", is("77000")))
                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 2);
            assertExist(entityName, 4);
            assertExist(entityName, 6);

            mvc.perform(
                            delete("/"+entityName+"/2,4,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-3);
            assertNotExist(entityName, 2);
            assertNotExist(entityName, 4);
            assertNotExist(entityName, 6);
        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2,4000,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 5);

            mvc.perform(
                            delete("/"+entityName+"/5")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);
            assertNotExist(entityName, 5);

        }

        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "firstName":"Fleur"
                                            }
                                            """))
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":"2000",
                                            "firstName":"Fleur"
                                            }
                                            """))
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                    get("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(6)))
                    .andExpect(jsonPath("$.firstName", is("Lyndsey")))
                    .andExpect(jsonPath("$.lastName", is("Bean")))
                    .andExpect(jsonPath("$.phone", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$.email", is("lyndsey.bean@hotmail.com")))
                    .andExpect(jsonPath("$.street", is("769 West Road ")))
                    .andExpect(jsonPath("$.city", is("Fairport")))
                    .andExpect(jsonPath("$.state", is("NY")))
                    .andExpect(jsonPath("$.zipCode", is("14450")))
                    .andExpect(jsonPath("$.orders", IsCollectionWithSize.hasSize(3)))
                    .andExpect(jsonPath("$.orders[0].id", is(1059)))
                    .andExpect(jsonPath("$.orders[0].status", is(4)))
                    .andExpect(jsonPath("$.orders[0].orderDate", is("2017-08-14")))
                    .andExpect(jsonPath("$.orders[0].requiredDate", is("2017-08-17")))
                    .andExpect(jsonPath("$.orders[0].shippedDate", is("2017-08-17")))
                    .andExpect(jsonPath("$.orders[1].id", is(1611)))
                    .andExpect(jsonPath("$.orders[1].status", is(3)))
                    .andExpect(jsonPath("$.orders[1].orderDate", is("2018-09-06")))
                    .andExpect(jsonPath("$.orders[1].requiredDate", is("2018-09-06")))
                    .andExpect(jsonPath("$.orders[1].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$.orders[2].id", is(1592)))
                    .andExpect(jsonPath("$.orders[2].status", is(2)))
                    .andExpect(jsonPath("$.orders[2].orderDate", is("2018-04-27")))
                    .andExpect(jsonPath("$.orders[2].requiredDate", is("2018-04-27")))
                    .andExpect(jsonPath("$.orders[2].shippedDate", is(IsNull.nullValue())))
                ;

            mvc.perform(
                            put("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":"6",
                                            "firstName":"Fleur"
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);


            mvc.perform(
                            get("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(6)))
                    .andExpect(jsonPath("$.firstName", is("Fleur")))
                    .andExpect(jsonPath("$.lastName", is("Bean")))
                    .andExpect(jsonPath("$.phone", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$.email", is("lyndsey.bean@hotmail.com")))
                    .andExpect(jsonPath("$.street", is("769 West Road ")))
                    .andExpect(jsonPath("$.city", is("Fairport")))
                    .andExpect(jsonPath("$.state", is("NY")))
                    .andExpect(jsonPath("$.zipCode", is("14450")))
                    .andExpect(jsonPath("$.orders", IsCollectionWithSize.hasSize(3)))
                    .andExpect(jsonPath("$.orders[0].id", is(1059)))
                    .andExpect(jsonPath("$.orders[0].status", is(4)))
                    .andExpect(jsonPath("$.orders[0].orderDate", is("2017-08-14")))
                    .andExpect(jsonPath("$.orders[0].requiredDate", is("2017-08-17")))
                    .andExpect(jsonPath("$.orders[0].shippedDate", is("2017-08-17")))
                    .andExpect(jsonPath("$.orders[1].id", is(1611)))
                    .andExpect(jsonPath("$.orders[1].status", is(3)))
                    .andExpect(jsonPath("$.orders[1].orderDate", is("2018-09-06")))
                    .andExpect(jsonPath("$.orders[1].requiredDate", is("2018-09-06")))
                    .andExpect(jsonPath("$.orders[1].shippedDate", is(IsNull.nullValue())))
                    .andExpect(jsonPath("$.orders[2].id", is(1592)))
                    .andExpect(jsonPath("$.orders[2].status", is(2)))
                    .andExpect(jsonPath("$.orders[2].orderDate", is("2018-04-27")))
                    .andExpect(jsonPath("$.orders[2].requiredDate", is("2018-04-27")))
                    .andExpect(jsonPath("$.orders[2].shippedDate", is(IsNull.nullValue())))
            ;
        }
    }

}
