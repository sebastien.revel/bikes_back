package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.srevel.bikes.bean.key.OrderItemId;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("OrderItem Controller Test")
@Slf4j(topic = "OrderItemControllerIntegrationTest")
public class OrderItemControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "orderitems";
    int initialNb = 4722;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.orderId", is(1)))
                    .andExpect(jsonPath("$.id.itemId", is(1)))
                    .andExpect(jsonPath("$.quantity", is(1)))
                    .andExpect(jsonPath("$.listPrice", is(599.99)))
                    .andExpect(jsonPath("$.discount", is(0.20)))

                    .andExpect(jsonPath("$.product.id", is(20)))
                    .andExpect(jsonPath("$.product.productName", is("Electra Townie Original 7D EQ - Women's - 2016")))
                    .andExpect(jsonPath("$.product.brand.id", is(1)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Electra")))
                    .andExpect(jsonPath("$.product.category.id", is(3)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Cruisers Bicycles")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(599.99)))

            ;
        }


        @Test
        @DisplayName("getByIdDetailled")
        public void givenInitialization_whenGetOneExistDetailled_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/order/1/item/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.orderId", is(1)))
                    .andExpect(jsonPath("$.id.itemId", is(1)))
                    .andExpect(jsonPath("$.quantity", is(1)))
                    .andExpect(jsonPath("$.listPrice", is(599.99)))
                    .andExpect(jsonPath("$.discount", is(0.20)))

                    .andExpect(jsonPath("$.product.id", is(20)))
                    .andExpect(jsonPath("$.product.productName", is("Electra Townie Original 7D EQ - Women's - 2016")))
                    .andExpect(jsonPath("$.product.brand.id", is(1)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Electra")))
                    .andExpect(jsonPath("$.product.category.id", is(3)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Cruisers Bicycles")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(599.99)))
            ;
        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/20000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
            ;


            mvc.perform(
                            get("/"+entityName+"/1-30000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
            ;
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1-1,1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(2)))

                    .andExpect(jsonPath("$[0].id.orderId", is(1)))
                    .andExpect(jsonPath("$[0].id.itemId", is(1)))
                    .andExpect(jsonPath("$[0].quantity", is(1)))
                    .andExpect(jsonPath("$[0].listPrice", is(599.99)))
                    .andExpect(jsonPath("$[0].discount", is(0.20)))

                    .andExpect(jsonPath("$[0].product.id", is(20)))
                    .andExpect(jsonPath("$[0].product.productName", is("Electra Townie Original 7D EQ - Women's - 2016")))
                    .andExpect(jsonPath("$[0].product.brand.id", is(1)))
                    .andExpect(jsonPath("$[0].product.brand.brandName", is("Electra")))
                    .andExpect(jsonPath("$[0].product.category.id", is(3)))
                    .andExpect(jsonPath("$[0].product.category.categoryName", is("Cruisers Bicycles")))
                    .andExpect(jsonPath("$[0].product.modelYear", is(2016)))
                    .andExpect(jsonPath("$[0].product.listPrice", is(599.99)))


                    .andExpect(jsonPath("$[1].id.orderId", is(1)))
                    .andExpect(jsonPath("$[1].id.itemId", is(2)))
                    .andExpect(jsonPath("$[1].quantity", is(2)))
                    .andExpect(jsonPath("$[1].listPrice", is(1799.99)))
                    .andExpect(jsonPath("$[1].discount", is(0.07)))

                    .andExpect(jsonPath("$[1].product.id", is(8)))
                    .andExpect(jsonPath("$[1].product.productName", is("Trek Remedy 29 Carbon Frameset - 2016")))
                    .andExpect(jsonPath("$[1].product.brand.id", is(9)))
                    .andExpect(jsonPath("$[1].product.brand.brandName", is("Trek")))
                    .andExpect(jsonPath("$[1].product.category.id", is(6)))
                    .andExpect(jsonPath("$[1].product.category.categoryName", is("Mountain Bikes")))
                    .andExpect(jsonPath("$[1].product.modelYear", is(2016)))
                    .andExpect(jsonPath("$[1].product.listPrice", is(1799.99)))

                ;
        }

        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1-1,20000-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());

            mvc.perform(
                            get("/"+entityName+"/many/1-1,1-20000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                                {
                                                    "id": {
                                                        "orderId": 6,
                                                        "itemId": 7
                                                    },
                                                    "product": {
                                                        "id": 2
                                                    },
                                                    "quantity": 33,
                                                    "listPrice": 22.22,
                                                    "discount": 0.10
                                                }
                                            """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.orderId", is(6)))
                    .andExpect(jsonPath("$.id.itemId", is(7)))
                    .andExpect(jsonPath("$.product.id", is(2)))
                    .andExpect(jsonPath("$.quantity", is(33)))
                    .andExpect(jsonPath("$.listPrice", is(22.22)))
                    .andExpect(jsonPath("$.discount", is(0.10)))
                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, new OrderItemId(1L,1L));
            assertExist(entityName, new OrderItemId(1L,2L));

            mvc.perform(
                            delete("/"+entityName+"/1-1,1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-2);
            assertNotExist(entityName, new OrderItemId(1L,1L));
            assertNotExist(entityName, new OrderItemId(1L,2L));
        }


        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, new OrderItemId(1L,1L));

            mvc.perform(
                            delete("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);
            assertNotExist(entityName, new OrderItemId(1L,1L));

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/1-1,1-20000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());


            mvc.perform(
                            delete("/"+entityName+"/1-1,20000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/1-20000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
                    .andReturn();


            mvc.perform(
                            delete("/"+entityName+"/20000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
                    .andReturn();
            assertCount(entityName, initialNb);

        }
    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "quantity": 66
                                            }
                                            """))
                    .andExpect(status().isConflict());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.orderId", is(1)))
                    .andExpect(jsonPath("$.id.itemId", is(1)))
                    .andExpect(jsonPath("$.quantity", is(1)))
                    .andExpect(jsonPath("$.listPrice", is(599.99)))
                    .andExpect(jsonPath("$.discount", is(0.20)))

                    .andExpect(jsonPath("$.product.id", is(20)))
                    .andExpect(jsonPath("$.product.productName", is("Electra Townie Original 7D EQ - Women's - 2016")))
                    .andExpect(jsonPath("$.product.brand.id", is(1)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Electra")))
                    .andExpect(jsonPath("$.product.category.id", is(3)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Cruisers Bicycles")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(599.99)))

                ;

            mvc.perform(
                            put("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":{
                                                "orderId": 1,
                                                "itemId": 1
                                              },
                                            "quantity": 66
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);


            mvc.perform(
                            get("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.orderId", is(1)))
                    .andExpect(jsonPath("$.id.itemId", is(1)))
                    .andExpect(jsonPath("$.quantity", is(66)))
                    .andExpect(jsonPath("$.listPrice", is(599.99)))
                    .andExpect(jsonPath("$.discount", is(0.20)))

                    .andExpect(jsonPath("$.product.id", is(20)))
                    .andExpect(jsonPath("$.product.productName", is("Electra Townie Original 7D EQ - Women's - 2016")))
                    .andExpect(jsonPath("$.product.brand.id", is(1)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Electra")))
                    .andExpect(jsonPath("$.product.category.id", is(3)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Cruisers Bicycles")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(599.99)))
            ;
        }

        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1-20000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id": {
                                                    "storeId": 1,
                                                    "productId": 20000
                                                },
                                            "quantity":66
                                            }
                                            """))
                    .andExpect(status().isConflict());


            mvc.perform(
                            put("/"+entityName+"/20000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id": {
                                                    "storeId": 20000,
                                                    "productId": 1
                                                },
                                            "quantity":29
                                            }
                                            """))
                    .andExpect(status().isConflict());


            assertCount(entityName, initialNb);
        }

    }


    public void assertExist (String entity, OrderItemId id) throws Exception {
        mvc.perform(
                        get("/"+entity+"/"+id.getOrderId()+"-"+id.getItemId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id.orderId", is(id.getOrderId().intValue())))
                .andExpect(jsonPath("$.id.itemId", is(id.getItemId().intValue())))
        ;
    }

    public void assertNotExist (String entity, OrderItemId id) throws Exception {
        mvc.perform(
                        get("/"+entity+"/"+id.getOrderId()+"-"+id.getItemId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound());
    }

}
