
package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DisplayName("Product Controller Test")
@Slf4j(topic = "ProductControllerIntegrationTest")
public class ProductControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "products";
    int initialNb = 321;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(6)))
                    .andExpect(jsonPath("$.productName", is("Surly Ice Cream Truck Frameset - 2016")))
                    .andExpect(jsonPath("$.modelYear", is(2016)))
                    .andExpect(jsonPath("$.listPrice", is(469.99)))
                    .andExpect(jsonPath("$.brand.id", is(8)))
                    .andExpect(jsonPath("$.brand.brandName", is("Surly")))
                    .andExpect(jsonPath("$.category.id", is(6)))
                    .andExpect(jsonPath("$.category.categoryName", is("Mountain Bikes")))
                ;
        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/6,7")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(2)))

                    .andExpect(jsonPath("$[0].id", is(6)))
                    .andExpect(jsonPath("$[0].productName", is("Surly Ice Cream Truck Frameset - 2016")))
                    .andExpect(jsonPath("$[0].modelYear", is(2016)))
                    .andExpect(jsonPath("$[0].listPrice", is(469.99)))
                    .andExpect(jsonPath("$[0].brand.id", is(8)))
                    .andExpect(jsonPath("$[0].brand.brandName", is("Surly")))
                    .andExpect(jsonPath("$[0].category.id", is(6)))
                    .andExpect(jsonPath("$[0].category.categoryName", is("Mountain Bikes")))

                    .andExpect(jsonPath("$[1].id", is(7)))
                    .andExpect(jsonPath("$[1].productName", is("Trek Slash 8 27.5 - 2016")))
                    .andExpect(jsonPath("$[1].modelYear", is(2016)))
                    .andExpect(jsonPath("$[1].listPrice", is(3999.99)))
                    .andExpect(jsonPath("$[1].brand.id", is(9)))
                    .andExpect(jsonPath("$[1].brand.brandName", is("Trek")))
                    .andExpect(jsonPath("$[1].category.id", is(6)))
                    .andExpect(jsonPath("$[1].category.categoryName", is("Mountain Bikes")))
                ;
        }



        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/10,2000,55")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {


        @Test
        @DisplayName("Create With Id")
        public void givenInitialization_whenCreateWithId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                              "id":"1448",
                                              "productName": "NEW Trek 820 - 2022 !!!",
                                              "brand": {
                                                  "id": 2
                                              },
                                              "category": {
                                                  "id": 2
                                              },
                                              "modelYear": 2022,
                                              "listPrice": 666.66
                                          }
                                            """)
                    )
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                                   {
                                                          "productName": "NEW Trek 820 - 2022 !!!",
                                                          "brand": {
                                                              "id": 2
                                                          },
                                                          "category": {
                                                              "id": 2
                                                          },
                                                          "modelYear": 2022,
                                                          "listPrice": 666.66
                                                      }
                                            """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(initialNb+1)))
                    .andExpect(jsonPath("$.productName", is("NEW Trek 820 - 2022 !!!")))
                    .andExpect(jsonPath("$.modelYear", is(2022)))
                    .andExpect(jsonPath("$.listPrice", is(666.66)))
                    .andExpect(jsonPath("$.brand.id", is(2)))
                    .andExpect(jsonPath("$.category.id", is(2)))
                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 2);
            assertExist(entityName, 4);
            assertExist(entityName, 6);

            mvc.perform(
                            delete("/"+entityName+"/2,4,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-3);
            assertNotExist(entityName, 2);
            assertNotExist(entityName, 4);
            assertNotExist(entityName, 6);
        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2,4000,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 5);

            mvc.perform(
                            delete("/"+entityName+"/5")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);


            assertNotExist(entityName, 5);

        }

        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "modelYear":2022
                                            }
                                            """))
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/10000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":10000,
                                            "modelYear":2022
                                            }
                                            """))
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                    get("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(6)))
                    .andExpect(jsonPath("$.productName", is("Surly Ice Cream Truck Frameset - 2016")))
                    .andExpect(jsonPath("$.modelYear", is(2016)))
                    .andExpect(jsonPath("$.listPrice", is(469.99)))
                    .andExpect(jsonPath("$.brand.id", is(8)))
                    .andExpect(jsonPath("$.brand.brandName", is("Surly")))
                    .andExpect(jsonPath("$.category.id", is(6)))
                    .andExpect(jsonPath("$.category.categoryName", is("Mountain Bikes")))
                ;

            mvc.perform(
                            put("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":"6",
                                            "productName":"NEW  Surly Ice Cream Truck Frameset - 2016"
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);


            mvc.perform(
                            get("/"+entityName+"/6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(6)))
                    .andExpect(jsonPath("$.productName", is("NEW  Surly Ice Cream Truck Frameset - 2016")))
                    .andExpect(jsonPath("$.modelYear", is(2016)))
                    .andExpect(jsonPath("$.listPrice", is(469.99)))
                    .andExpect(jsonPath("$.brand.id", is(8)))
                    .andExpect(jsonPath("$.brand.brandName", is("Surly")))
                    .andExpect(jsonPath("$.category.id", is(6)))
                    .andExpect(jsonPath("$.category.categoryName", is("Mountain Bikes")))
            ;
        }
    }

}
