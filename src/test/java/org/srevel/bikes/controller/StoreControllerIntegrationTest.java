
package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Store Controller Test")
@Slf4j(topic = "StoreControllerIntegrationTest")
public class StoreControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "stores";
    int initialNb = 3;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(2)))
                    .andExpect(jsonPath("$.storeName", is("Baldwin Bikes")))
                    .andExpect(jsonPath("$.phone", is("(516) 379-8888")))
                    .andExpect(jsonPath("$.email", is("baldwin@bikes.shop")))
                    .andExpect(jsonPath("$.street", is("4200 Chestnut Lane")))
                    .andExpect(jsonPath("$.city", is("Baldwin")))
                    .andExpect(jsonPath("$.state", is("NY")))
                    .andExpect(jsonPath("$.zipCode", is("11432")))
                ;
        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1,2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(2)))

                    .andExpect(jsonPath("$[0].id", is(1)))
                    .andExpect(jsonPath("$[0].storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$[0].phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$[0].email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$[0].street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$[0].city", is("Santa Cruz")))
                    .andExpect(jsonPath("$[0].state", is("CA")))
                    .andExpect(jsonPath("$[0].zipCode", is("95060")))

                    .andExpect(jsonPath("$[1].id", is(2)))
                    .andExpect(jsonPath("$[1].storeName", is("Baldwin Bikes")))
                    .andExpect(jsonPath("$[1].phone", is("(516) 379-8888")))
                    .andExpect(jsonPath("$[1].email", is("baldwin@bikes.shop")))
                    .andExpect(jsonPath("$[1].street", is("4200 Chestnut Lane")))
                    .andExpect(jsonPath("$[1].city", is("Baldwin")))
                    .andExpect(jsonPath("$[1].state", is("NY")))
                    .andExpect(jsonPath("$[1].zipCode", is("11432")))
                ;
        }


        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1,55")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {


        @Test
        @DisplayName("Create With Id")
        public void givenInitialization_whenCreateWithId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                              "id":"1448",
                                                "storeName": "Yebles Bikes",
                                                "phone": "(831) 12345678",
                                                "email": "yebles@bikes.shop",
                                                "street": "3700 Rue de Paris",
                                                "city": "Yebles",
                                                "state": "FR",
                                                "zipCode": "77000"
                                            }
                                            """)
                    )
                    .andExpect(status().isConflict())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "storeName": "Yebles Bikes",
                                                "phone": "(831) 12345678",
                                                "email": "yebles@bikes.shop",
                                                "street": "3700 Rue de Paris",
                                                "city": "Yebles",
                                                "state": "FR",
                                                "zipCode": "77000"
                                            }
                                            """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(initialNb+1)))
                    .andExpect(jsonPath("$.storeName", is("Yebles Bikes")))
                    .andExpect(jsonPath("$.phone", is("(831) 12345678")))
                    .andExpect(jsonPath("$.email", is("yebles@bikes.shop")))
                    .andExpect(jsonPath("$.street", is("3700 Rue de Paris")))
                    .andExpect(jsonPath("$.city", is("Yebles")))
                    .andExpect(jsonPath("$.state", is("FR")))
                    .andExpect(jsonPath("$.zipCode", is("77000")))
                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 1);
            assertExist(entityName, 3);

            mvc.perform(
                            delete("/"+entityName+"/1,3")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-2);
            assertNotExist(entityName, 1);
            assertNotExist(entityName, 3);
        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2,4000,6")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, 1);

            mvc.perform(
                            delete("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);
            assertNotExist(entityName, 1);

        }

        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
                    .andReturn();

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "storeName": "Santa Cruz Bikes  !!!!!!!!",
                                                "phone": "(831) 12345678",
                                                "email": "sebrevel@bikes.shop",
                                                "street": "3700 rue de Paris",
                                                "city": "Yèbles",
                                                "state": "FR",
                                                "zipCode": "77000"
                                            }
                                            """))
                    .andExpect(status().isConflict());

            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                                "id":1000,
                                                "storeName": "Santa Cruz Bikes  !!!!!!!!",
                                                "phone": "(831) 12345678",
                                                "email": "sebrevel@bikes.shop",
                                                "street": "3700 rue de Paris",
                                                "city": "Yèbles",
                                                "state": "FR",
                                                "zipCode": "77000"                                            
                                            }
                                            """))
                    .andExpect(status().isNotFound());

            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                    get("/"+entityName+"/2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(2)))
                    .andExpect(jsonPath("$.storeName", is("Baldwin Bikes")))
                    .andExpect(jsonPath("$.phone", is("(516) 379-8888")))
                    .andExpect(jsonPath("$.email", is("baldwin@bikes.shop")))
                    .andExpect(jsonPath("$.street", is("4200 Chestnut Lane")))
                    .andExpect(jsonPath("$.city", is("Baldwin")))
                    .andExpect(jsonPath("$.state", is("NY")))
                    .andExpect(jsonPath("$.zipCode", is("11432")))
                ;

            mvc.perform(
                            put("/"+entityName+"/2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":"2",
                                            "storeName":"Yebles Bikes"
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName+"/2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(2)))
                    .andExpect(jsonPath("$.storeName", is("Yebles Bikes")))
                    .andExpect(jsonPath("$.phone", is("(516) 379-8888")))
                    .andExpect(jsonPath("$.email", is("baldwin@bikes.shop")))
                    .andExpect(jsonPath("$.street", is("4200 Chestnut Lane")))
                    .andExpect(jsonPath("$.city", is("Baldwin")))
                    .andExpect(jsonPath("$.state", is("NY")))
                    .andExpect(jsonPath("$.zipCode", is("11432")))
            ;
        }
    }

}
