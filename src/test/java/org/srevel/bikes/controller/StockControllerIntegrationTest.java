package org.srevel.bikes.controller;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.srevel.bikes.bean.key.StockId;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Stock Controller Test")
@Slf4j(topic = "StockControllerIntegrationTest")
public class StockControllerIntegrationTest extends  GenericControllerIntegrationTest {

    String entityName = "stocks";
    int initialNb = 939;

    @Nested
    @DisplayName("GetAndCount tests")
    class GetAndCountTests {

        @Test
        @DisplayName("Count")
        public void givenInitialization_whenCount_thenReturnInitialNb()
                throws Exception {
            assertCount(entityName, initialNb);
        }

        @Test
        @DisplayName("getAll")
        public void givenInitialization_whenGetAll_thenReturnInitialNbItems () throws Exception {
            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(initialNb)));
        }

        @Test
        @DisplayName("getById")
        public void givenInitialization_whenGetOneExist_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.storeId", is(1)))
                    .andExpect(jsonPath("$.id.productId", is(2)))
                    .andExpect(jsonPath("$.quantity", is(5)))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$.store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$.store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$.store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$.store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$.store.state", is("CA")))
                    .andExpect(jsonPath("$.store.zipCode", is("95060")))

                    .andExpect(jsonPath("$.product.id", is(2)))
                    .andExpect(jsonPath("$.product.productName", is("Ritchey Timberwolf Frameset - 2016")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(749.99)))
                    .andExpect(jsonPath("$.product.brand.id", is(5)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Ritchey")))
                    .andExpect(jsonPath("$.product.category.id", is(6)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Mountain Bikes")))

            ;
        }


        @Test
        @DisplayName("getByIdDetailled")
        public void givenInitialization_whenGetOneExistDetailled_thenReturnOneItem () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/store/1/product/2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.storeId", is(1)))
                    .andExpect(jsonPath("$.id.productId", is(2)))
                    .andExpect(jsonPath("$.quantity", is(5)))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$.store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$.store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$.store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$.store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$.store.state", is("CA")))
                    .andExpect(jsonPath("$.store.zipCode", is("95060")))

                    .andExpect(jsonPath("$.product.id", is(2)))
                    .andExpect(jsonPath("$.product.productName", is("Ritchey Timberwolf Frameset - 2016")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(749.99)))
                    .andExpect(jsonPath("$.product.brand.id", is(5)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Ritchey")))
                    .andExpect(jsonPath("$.product.category.id", is(6)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Mountain Bikes")))

            ;
        }



        @Test
        @DisplayName("searchByStoreId")
        public void givenInitialization_whenSearchByStoreId_thenReturn313Items () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/store/3")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(313)))
            ;
        }

        @Test
        @DisplayName("searchByProductId")
        public void givenInitialization_whenSearchByProductId_thenReturn3Items () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/product/32")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(3)))
            ;
        }


        @Test
        @DisplayName("getById Not Exist")
        public void givenInitialization_whenGetOneNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/200-3")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
            ;


            mvc.perform(
                            get("/"+entityName+"/1-3000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound())
            ;
        }

        @Test
        @DisplayName("getAllByIds")
        public void givenInitialization_whenGetAllByIds_thenReturnManyItems () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1-1,1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", IsCollectionWithSize.hasSize(2)))

                    .andExpect(jsonPath("$[0].id.storeId", is(1)))
                    .andExpect(jsonPath("$[0].id.productId", is(1)))
                    .andExpect(jsonPath("$[0].quantity", is(27)))

                    .andExpect(jsonPath("$[0].store.id", is(1)))
                    .andExpect(jsonPath("$[0].store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$[0].store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$[0].store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$[0].store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$[0].store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$[0].store.state", is("CA")))
                    .andExpect(jsonPath("$[0].store.zipCode", is("95060")))

                    .andExpect(jsonPath("$[0].product.id", is(1)))
                    .andExpect(jsonPath("$[0].product.productName", is("Trek 820 - 2016")))
                    .andExpect(jsonPath("$[0].product.modelYear", is(2016)))
                    .andExpect(jsonPath("$[0].product.listPrice", is(379.99)))
                    .andExpect(jsonPath("$[0].product.brand.id", is(9)))
                    .andExpect(jsonPath("$[0].product.brand.brandName", is("Trek")))
                    .andExpect(jsonPath("$[0].product.category.id", is(6)))
                    .andExpect(jsonPath("$[0].product.category.categoryName", is("Mountain Bikes")))


                    .andExpect(jsonPath("$[1].id.storeId", is(1)))
                    .andExpect(jsonPath("$[1].id.productId", is(2)))
                    .andExpect(jsonPath("$[1].quantity", is(5)))

                    .andExpect(jsonPath("$[1].store.id", is(1)))
                    .andExpect(jsonPath("$[1].store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$[1].store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$[1].store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$[1].store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$[1].store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$[1].store.state", is("CA")))
                    .andExpect(jsonPath("$[1].store.zipCode", is("95060")))

                    .andExpect(jsonPath("$[1].product.id", is(2)))
                    .andExpect(jsonPath("$[1].product.productName", is("Ritchey Timberwolf Frameset - 2016")))
                    .andExpect(jsonPath("$[1].product.modelYear", is(2016)))
                    .andExpect(jsonPath("$[1].product.listPrice", is(749.99)))
                    .andExpect(jsonPath("$[1].product.brand.id", is(5)))
                    .andExpect(jsonPath("$[1].product.brand.brandName", is("Ritchey")))
                    .andExpect(jsonPath("$[1].product.category.id", is(6)))
                    .andExpect(jsonPath("$[1].product.category.categoryName", is("Mountain Bikes")))
                ;
        }

        @Test
        @DisplayName("getAllByIds Not Exist")
        public void givenInitialization_whenGetAllByIdsNotExist_thenReturnFailed () throws Exception {

            mvc.perform(
                            get("/"+entityName+"/many/1-1,200-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());

            mvc.perform(
                            get("/"+entityName+"/many/1-1,1-20000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
        }
    }


    @Nested
    @DisplayName("Create tests")
    class CreateTests {

        @Test
        @DisplayName("Create")
        public void givenInitialization_whenCreate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            post("/"+entityName)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                               {
                                              "id": {
                                                        "storeId": 3,
                                                        "productId": 315
                                                    },
                                                "quantity": 666
                                               }
                                            """)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.storeId", is(3)))
                    .andExpect(jsonPath("$.id.productId", is(315)))
                    .andExpect(jsonPath("$.quantity", is(666)))
                    .andReturn();

            assertCount(entityName, initialNb+1);

        }
    }

    @Nested
    @DisplayName("Delete tests")
    class DeleteTests {

        @Test
        @DisplayName("DeleteAllByIds")
        public void givenInitialization_whenDeleteAllByIds_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, new StockId(1L,1L));
            assertExist(entityName, new StockId(1L,2L));

            mvc.perform(
                            delete("/"+entityName+"/1-1,1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-2);
            assertNotExist(entityName, new StockId(1L,1L));
            assertNotExist(entityName, new StockId(1L,2L));
        }


        @Test
        @DisplayName("DeleteOne")
        public void givenInitialization_whenDeleteOneExist_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);
            assertExist(entityName, new StockId(1L,1L));

            mvc.perform(
                            delete("/"+entityName+"/1-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, initialNb-1);
            assertNotExist(entityName, new StockId(1L,1L));

        }

        @Test
        @DisplayName("DeleteAll")
        public void givenInitialization_whenDeleteAll_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertCount(entityName, 0);

        }

        @Test
        @DisplayName("DeleteAllByIds Not Exist")
        public void givenInitialization_whenDeleteAllByIdsNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/1-1,1-20000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());


            mvc.perform(
                            delete("/"+entityName+"/1-1,20000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isNotFound());
            assertCount(entityName, initialNb);

        }


        @Test
        @DisplayName("DeleteOne Not Exist")
        public void givenInitialization_whenDeleteOneNotExist_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            delete("/"+entityName+"/1-2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().is4xxClientError())
                    .andReturn();


            mvc.perform(
                            delete("/"+entityName+"/2000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().is4xxClientError())
                    .andReturn();
            assertCount(entityName, initialNb);

        }
    }


    @Nested
    @DisplayName("Update tests")
    class UpdateTests {

        @Test
        @DisplayName("Update without Id")
        public void givenInitialization_whenUpdateWithoutId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "categoryName": "Cat Bikes"
                                            }
                                            """))
                    .andExpect(status().is4xxClientError());

            assertCount(entityName, initialNb);

        }

        @Test
        @DisplayName("Update")
        public void givenInitialization_whenUpdate_thenReturnOk()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            get("/"+entityName+"/1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.storeId", is(1)))
                    .andExpect(jsonPath("$.id.productId", is(2)))
                    .andExpect(jsonPath("$.quantity", is(5)))

                    .andExpect(jsonPath("$.store.id", is(1)))
                    .andExpect(jsonPath("$.store.storeName", is("Santa Cruz Bikes")))
                    .andExpect(jsonPath("$.store.phone", is("(831) 476-4321")))
                    .andExpect(jsonPath("$.store.email", is("santacruz@bikes.shop")))
                    .andExpect(jsonPath("$.store.street", is("3700 Portola Drive")))
                    .andExpect(jsonPath("$.store.city", is("Santa Cruz")))
                    .andExpect(jsonPath("$.store.state", is("CA")))
                    .andExpect(jsonPath("$.store.zipCode", is("95060")))

                    .andExpect(jsonPath("$.product.id", is(2)))
                    .andExpect(jsonPath("$.product.productName", is("Ritchey Timberwolf Frameset - 2016")))
                    .andExpect(jsonPath("$.product.modelYear", is(2016)))
                    .andExpect(jsonPath("$.product.listPrice", is(749.99)))
                    .andExpect(jsonPath("$.product.brand.id", is(5)))
                    .andExpect(jsonPath("$.product.brand.brandName", is("Ritchey")))
                    .andExpect(jsonPath("$.product.category.id", is(6)))
                    .andExpect(jsonPath("$.product.category.categoryName", is("Mountain Bikes")))

                ;

            mvc.perform(
                            put("/"+entityName+"/1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id":{
                                                  "storeId": 1,
                                                  "productId": 2
                                              },
                                            "quantity": 66
                                            }
                                            """))
                    .andExpect(status().isOk());

            assertCount(entityName, initialNb);


            mvc.perform(
                            get("/"+entityName+"/1-2")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id.storeId", is(1)))
                    .andExpect(jsonPath("$.id.productId", is(2)))
                    .andExpect(jsonPath("$.quantity", is(66)))
            ;
        }

        @Test
        @DisplayName("Update with Unknown Id")
        public void givenInitialization_whenUpdateWithUnknownId_thenReturnFailed()
                throws Exception {

            assertCount(entityName, initialNb);

            mvc.perform(
                            put("/"+entityName+"/1-2000")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id": {
                                                    "storeId": 1,
                                                    "productId": 2000
                                                },
                                            "quantity":29
                                            }
                                            """))
                    .andExpect(status().isNotFound());


            mvc.perform(
                            put("/"+entityName+"/2000-1")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .characterEncoding("utf-8")
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content("""
                                            {
                                            "id": {
                                                    "storeId": 2000,
                                                    "productId": 1
                                                },
                                            "quantity":29
                                            }
                                            """))
                    .andExpect(status().isNotFound());


            assertCount(entityName, initialNb);
        }

    }


    public void assertExist (String entity, StockId id) throws Exception {
        mvc.perform(
                        get("/"+entity+"/"+id.getStoreId()+"-"+id.getProductId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id.storeId", is(id.getStoreId().intValue())))
                .andExpect(jsonPath("$.id.productId", is(id.getProductId().intValue())))
        ;
    }

    public void assertNotExist (String entity, StockId id) throws Exception {
        mvc.perform(
                        get("/"+entity+"/"+id.getStoreId()+"-"+id.getProductId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is4xxClientError());
    }
}
